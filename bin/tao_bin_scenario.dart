import 'dart_app_calculate.dart'; //for access Calculate
import 'dart:io';
import 'dart:math';

class CatagoryMenu {
  Map<int, String> catgMap = {
    1: 'Coffee',
    2: 'Tea',
    3: 'Milk Cocoa and Caramel',
    4: 'Protein Shake',
    5: 'Soda and Others',
    6: 'Recommended Menu'
  };
  void getCatagoryMenu() {
    print('Catagory Menu of TAO BIN:');
    catgMap.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }
}

class CoffeeMenu {
  Map<int, String> mapCoffee = {
    1: 'Espresso (Hot ฿25)',
    2: 'Double Espresso (Hot ฿35)',
    3: 'Americano (Hot ฿30 / Iced ฿35)',
    4: 'Espresso Shot Blue Daddy (Hot ฿55)',
    5: 'Americano Soda (Iced ฿40)',
    6: 'Americano Blue Daddy (Hot ฿60 / Iced ฿65)',
    7: 'Blue Daddy Almost Dirty (Iced ฿70)',
    8: 'Iced Espresso (Iced ฿45 / Smoothie ฿50)',
    9: 'Cafe Latte (Hot ฿35 / Iced ฿40 / Smoothie ฿45)',
    10: 'Cappuccino (Hot ฿35 / Iced ฿40 / Smoothie ฿45)',
    11: 'Mocha (Hot ฿40 / Iced ฿45 / Smoothie ฿50)',
    12: 'Caramel Latte (Hot ฿40 / Iced ฿45 / Smoothie ฿50)',
    13: 'Matcha Latte (Hot ฿45 / Iced ฿50 / Smoothie ฿55)',
    14: 'Brown Sugar Latte (Hot ฿40 / Iced ฿45 / Smoothie ฿50)',
    15: 'Thai Tea Cafe Latte (Hot ฿40 / Iced ฿45)',
    16: 'Taiwanese Tea Cafe Latte (Hot ฿40 / Iced ฿45 / Smoothie ฿50)',
    17: 'Lychee Americano (Hot ฿30 / Iced ฿35)'
  };
  void coffeeMenu() {
    print('Coffee Menu of TAO BIN:');
    mapCoffee.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }
}

class TeaMenu {
  Map<int, String> mapTea = {
    1: 'Chrysanthemum Tea (Hot ฿20 / Iced ฿25 / Smoothie ฿30)',
    2: 'Ginger Tea (Hot ฿20 / Iced ฿25 / Smoothie ฿30)',
    3: 'Lime Ginger Tea (Hot ฿20)',
    4: 'Thai Milk Tea (Hot ฿35 / Iced ฿40 / Smoothie ฿45)',
    5: 'Taiwanese Tea (Hot ฿40 / Iced ฿45 / Smoothie ฿50)',
    6: 'Matcha Latte (Hot ฿40 / Iced ฿45 / Smoothie ฿50)',
    7: 'Japanese Green Tea (Hot ฿20 / Iced ฿30 / Smoothie ฿35)',
    8: 'Lime Japanese Green Tea (Hot ฿25 / Iced ฿35 / Smoothie ฿40)',
    9: 'Black Tea (Hot ฿25 / Iced ฿30)',
    10: 'Lime Black Tea (Hot ฿25 / Iced ฿30)',
    11: 'Tiwanese Black Tea (Hot ฿20 / Iced ฿25)',
    12: 'Lime Tiwanese Black Tea (Hot ฿25 / Iced ฿30)'
  };
  void teaMenu() {
    print('Tea Menu of TAO BIN:');
    mapTea.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }
}

class MilkCCMenu {
  Map<int, String> mapMilkCC = {
    1: 'Milk (Hot ฿30 / Iced ฿35 / Smoothie ฿40)',
    2: 'Caramel Milk (Hot ฿35 / Iced ฿40 / Smoothie ฿45)',
    3: 'Caramel Cocoa (Hot ฿35 / Iced ฿40 / Smoothie ฿45)',
    4: 'Brown Sugar Milk (Hot ฿35 / Iced ฿40 / Smoothie ฿45)',
    5: 'Pink Milk (Iced ฿35 / Smoothie ฿40)',
    6: 'Cocoa (Hot ฿30 / Iced ฿35 / Smoothie ฿40)',
    7: 'Smoothie Strawberry Cocoa (Smoothie ฿45)',
    8: 'Smoothie Strawberry Milk (Smoothie ฿45)',
    9: 'Smoothie Volcano Oreo (Smoothie ฿55)'
  };
  void milkCCMenu() {
    print('Milk Cocoa and Caramel Menu of TAO BIN:');
    mapMilkCC.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }
}

class PTShakeMenu {
  Map<int, String> mapPtShake = {
    1: 'Macha Protein Shake (Iced ฿65)',
    2: 'Cocoa Protein Shake (Iced ฿65)',
    3: 'Strawberry Protein Shake (Iced ฿65)',
    4: 'Espresso Protein Shake (Iced ฿65)',
    5: 'Thai Milk Tea Protein Shake (Iced ฿65)',
    6: 'Brown Sugar Protein Shake (Iced ฿65)',
    7: 'Taiwanese Tea Protein Shake (Iced ฿65)',
    8: 'Caramel Protein Shake (Iced ฿65)',
    9: 'Milk Protein Shake (Iced ฿65)',
    10: 'Pure Protein Shake (Iced ฿60)'
  };
  void ptShakeMenu() {
    print('Protein Shake Menu of TAO BIN:');
    mapPtShake.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }
}

class SodaAOthMenu {
  Map<int, String> mapSodaFt = {
    1: 'Pepsi (Iced ฿15 / Smoothie ฿20)',
    2: 'Powerful Turtle (Iced ฿15)',
    3: 'Limenade Soda (Iced ฿20 / Smoothie ฿25)',
    4: 'Lychee Soda (Iced ฿25 / Smoothie ฿30)',
    5: 'Strawberry Soda (Iced ฿35 / Smoothie ฿40)',
    6: 'Sala Soda (Iced ฿20 / Smoothie ฿25)',
    7: 'Lime Sala Soda (Iced ฿25 / Smoothie ฿30)',
    8: 'Plum Soda (Iced ฿25 / Smoothie ฿30)',
    9: 'Lime Plum Soda (Iced ฿25 / Smoothie ฿30)',
    10: 'Plum Pepsi (Iced ฿20 / Smoothie ฿25)',
    11: 'Ginger Soda (Iced ฿25 / Smoothie ฿30)',
    12: 'Lime Ginger Soda (Iced ฿25 / Smoothie ฿30)',
    13: 'Soda (Iced ฿10)',
    14: 'Iced Soda (Iced ฿10)',
    15: 'SaLa (Iced ฿20 / Smoothie ฿25)',
    16: 'Limenade (Hot ฿20 / Iced ฿25 / Smoothie ฿30)',
    17: 'Plum (Iced ฿25 / Smoothie ฿30)',
    18: 'Lychee (Iced ฿25 / Smoothie ฿30)',
    19: 'Strawberry (Smoothie ฿35)',
    20: 'Iced (Iced ฿10)',
    21: 'Water (Hot ฿10 / Iced ฿10)'
  };
  void sodaFtMenu() {
    print('Soda and Fruity Menu of TAO BIN:');
    mapSodaFt.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }
}

class RecommendMenu {
  Map<int, String> mapRecm = {
    1: 'Espresso (Hot ฿25)',
    2: 'Americano (Hot ฿30 / Iced ฿35)',
    3: 'Cafe Latte (Hot ฿35 / Iced ฿40 / Smoothie ฿45)',
    4: 'Thai Milk Tea (Hot ฿35 / Iced ฿40 / Smoothie ฿45)',
    5: 'Cocoa (Hot ฿30 / Iced ฿35 / Smoothie ฿40)',
    6: 'Caramel Milk (Hot ฿35 / Iced ฿40 / Smoothie ฿45)',
    7: 'Pink Milk (Iced ฿35 / Smoothie ฿40)',
    8: 'Smoothie Strawberry Milk (Smoothie ฿45)',
    9: 'Smoothie Volcano Oreo (Smoothie ฿55)',
    10: 'Pepsi (Iced ฿15 / Smoothie ฿20)',
    11: 'Plum Pepsi (Iced ฿20 / Smoothie ฿25)',
    12: 'Powerful Turtle (Iced ฿15)',
    13: 'Plum Lime Soda (Iced ฿25 / Smoothie ฿30)'
  };
  void recommendationMenu() {
    print('Recommended Menu of TAO BIN:');
    mapRecm.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }
}

class LevelOfSweet {
  Map<int, String> mapSweet = {
    1: 'No Sugar',
    2: 'Little Sweet',
    3: 'Medium',
    4: 'So Sweet',
    5: 'Very Sweet; Sweet 3 Worlds'
  };
}

class Payment {
  Map<int, String> mapPayment = {
    1: 'Cash',
    2: 'Scan QR',
    3: 'TAO BIN Credit',
    4: 'Use/View Coupon',
    5: 'E-Wallet',
    6: 'Other Promotions'
  };
}

class LvlArmt {
  Map<int, String> lvlArmt = {
    1: 'Shyny Turtle',
    2: 'Totter Turtle',
    3: 'Winged Turtle',
    4: 'Oldster Turtle',
  };
  void lvlArmatureDescribe() {
    print('Benefit\' Member of TAO BIN:');
    lvlArmt.forEach((key, value) {
      print('$key : $value');
      describeBf(key);
    });
    print('----------');
    print(
        'When collecting 30 turtle shells, TAO BIN will send a coupon to redeem. \n1 free drink immediately via SMS');
    print('----------');
  }

  void describeBf(int lv) {
    switch (lv) {
      case 1:
        print(
            'when collecting turtle shells with our mobile number, \nyou will receive 1 armature for every 20 baht purchase.');
        break;
      case 2:
        print(
            'When registering as a flying turtle member at LINE@TAOBIN, you will receive \n1 armature with every purchase of 15 baht and get the right to redeem a drink \nwith a discount of 50 in the month of birth 1 time.');
        break;
      case 3:
        print(
            'When a total of 150 turtle shells accumulated, you will receive \n1 armature. For every purchase of 15 baht, get a free drink coupon. \nWhen the status is postponed as scheduled and get a free redemption right \nin the month of birth 1 time');
        break;
      case 4:
        print(
            'When you have accumulated 350 turtle shells, you will receive \na free drink coupon. When the status is postponed as scheduled And get \na free redemption in the month of birth 1 time. Specially, \nreceive a New Year gift from TAO BIN.');
        break;
    }
  }
}

class ProcessTAOBIN {
  CatagoryMenu catgMenu = CatagoryMenu();
  CoffeeMenu cfMenu = CoffeeMenu();
  TeaMenu tMenu = TeaMenu();
  MilkCCMenu mccMenu = MilkCCMenu();
  PTShakeMenu ptMenu = PTShakeMenu();
  SodaAOthMenu sdoMenu = SodaAOthMenu();
  RecommendMenu rcMenu = RecommendMenu();
  LevelOfSweet lvlSweet = LevelOfSweet();
  Payment payment = Payment();
  LvlArmt lvlArmts = LvlArmt();

  Map<String, String?> mapOrder = {
    'Catagory Menu': '',
    'Menu': '',
    'Type of Menu': '',
    'Level of Sweet': '',
    'Bulb': '',
    'Lid': '',
    'Price': '',
    'Payment': '',
    'Pay': '',
    'Change': '',
    'Number of Armature': '',
    'Level of Armature': ''
  };

  void getOrder() {
    print('----------');
    print('Your Order: ');
    mapOrder.forEach((key, value) {
      if (mapOrder[key] != '') {
        print('$key : $value');
      }
    });
    print('----------');
  }

  void getMenu(int categoryMenu) {
    //String catgMapS = catgMap[categoryMenu] as String;
    mapOrder.update('Catagory Menu', (value) => catgMenu.catgMap[categoryMenu]);
    switch (categoryMenu) {
      case 1:
        cfMenu.coffeeMenu();
        int menu = choosingMenu();
        // String mapCoffeeS = mapCoffee[menu] as String;
        mapOrder.update('Menu', (value) => cfMenu.mapCoffee[menu]);
        getPropMenu(cfMenu.mapCoffee[menu]);
        break;
      case 2:
        tMenu.teaMenu();
        int menu = choosingMenu();
        mapOrder.update('Menu', (value) => tMenu.mapTea[menu]);
        getPropMenu(tMenu.mapTea[menu]);
        break;
      case 3:
        mccMenu.milkCCMenu();
        int menu = choosingMenu();
        mapOrder.update('Menu', (value) => mccMenu.mapMilkCC[menu]);
        getPropMenu(mccMenu.mapMilkCC[menu]);
        break;
      case 4:
        ptMenu.ptShakeMenu();
        int menu = choosingMenu();
        mapOrder.update('Menu', (value) => ptMenu.mapPtShake[menu]);
        getPropMenu(ptMenu.mapPtShake[menu]);
        break;
      case 5:
        sdoMenu.sodaFtMenu();
        int menu = choosingMenu();
        mapOrder.update('Menu', (value) => sdoMenu.mapSodaFt[menu]);
        getPropMenu(sdoMenu.mapSodaFt[menu]);
        break;
      case 6:
        rcMenu.recommendationMenu();
        int menu = choosingMenu();
        mapOrder.update('Menu', (value) => rcMenu.mapRecm[menu]);
        getPropMenu(rcMenu.mapRecm[menu]);
        break;
    }
  }

  int choosingMenu() {
    int menu = int.parse(stdin.readLineSync()!);
    print('----------');
    return menu;
  }

  void getPropMenu(String? menu) {
    print('type of Menu: ');
    getTypeMenu(menu);
    getLevelSweet();
    getBulb();
    getLid();
  }

  void getTypeMenu(String? menu) {
    // print(menu);
    List<String> Type = getTypeFromMenu(menu);
    if (Type.length > 1) {
      Map<int, String> m = showAllTypeMenu(Type);
      int cType = choosingType();
      mapOrder.update('Type of Menu', (value) => m[cType]);
      getPrice(m[cType]);
    } else {
      print(Type[0]);
      mapOrder.update('Type of Menu', (value) => Type[0]);
      getPrice(Type[0]);
    }
  }

  int choosingType() {
    int cType = int.parse(stdin.readLineSync()!);
    print('----------');
    return cType;
  }

  Map<int, String> showAllTypeMenu(List<String> Type) {
    Map<int, String> m = Type.asMap();
    m.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
    return m;
  }

  List<String> getTypeFromMenu(String? menu) {
    String Menu = menu as String;
    Menu = Menu.replaceAll(')', '');
    var s = Menu.split('(');
    // print(s);
    String type = s[1];
    var Type = type.split(' / ');
    return Type;
  }

  void getPrice(String? type) {
    String Type = type as String;
    var s = Type.split(' ');
    // print(s);
    mapOrder.update('Price', (value) => s[1]);
  }

  void getLevelSweet() {
    print('Level\'s sweet of TAO BIN: ');
    lvlSweet.mapSweet.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
    int cSweet = choosingLvlSweet();
    mapOrder.update('Level of Sweet', (value) => lvlSweet.mapSweet[cSweet]);
  }

  int choosingLvlSweet() {
    int cSweet = int.parse(stdin.readLineSync()!);
    print('----------');
    return cSweet;
  }

  Map<int, String> mapYN = {1: 'Yes', 2: 'No'};

  void getBulb() {
    print('Have you want a bulb from TAO BIN: ');
    mapYN.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
    int cBulb = askBulb();
    mapOrder.update('Bulb', (value) => mapYN[cBulb]);
  }

  int askBulb() {
    int cBulb = int.parse(stdin.readLineSync()!);
    print('----------');
    return cBulb;
  }

  void getLid() {
    print('Have you want a lid from TAO BIN: ');
    mapYN.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
    int cLid = askLid();
    mapOrder.update('Lid', (value) => mapYN[cLid]);
  }

  int askLid() {
    int cLid = int.parse(stdin.readLineSync()!);
    print('----------');
    return cLid;
  }

  void getPayment() {
    print('Which you want to pay on TAO BIN: ');
    payment.mapPayment.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
    int cPayment = choosingPayment();
    mapOrder.update('Payment', (value) => payment.mapPayment[cPayment]);
    Pay(cPayment);
  }

  int choosingPayment() {
    int cPayment = int.parse(stdin.readLineSync()!);
    print('----------');
    return cPayment;
  }

  void Pay(int cPayment) {
    if (cPayment == 1) {
      print('Please input your money in box of TAO BIN: ');
      String input = payBill();
      double priceMenu = getNumFromPrice();
      double pay = double.parse(input);
      while (priceMenu > pay) {
        double cc = calNotEnoughPart(priceMenu, pay);
        pay = getMoreMoneyUntilEnough(cc, pay);
      }
      mapOrder.update('Pay', (value) => pay.toString());
      Calculate c = Calculate(pay, '-', priceMenu);
      mapOrder.update('Change', (value) => c.calculate().toString());
    } else {
      mapOrder.update('Pay', (value) => mapOrder['Price']);
      mapOrder.update('Change', (value) => '-');
    }
  }

  double calNotEnoughPart(double priceMenu, double pay) {
    Calculate c = Calculate(priceMenu, '-', pay);
    double cc = c.calculate();
    return cc;
  }

  double getMoreMoneyUntilEnough(double cc, double pay) {
    print('Please input your money more than or equal $cc');
    double addP = double.parse(stdin.readLineSync()!);
    pay += addP;
    return pay;
  }

  double getNumFromPrice() {
    String p = mapOrder['Price'] as String;
    String pm = p.substring(1);
    double priceMenu = double.parse(pm);
    return priceMenu;
  }

  String payBill() {
    String input = stdin.readLineSync()!;
    print('----------');
    return input;
  }

  Map<int, String> status = {1: 'Member LINE@TAOBIN', 2: '-'};

  void getArmatures(String tel) {
    int valueN = useAsCumtomPoints();
    String? sts = status[2];
    showStatus(sts, tel);
    String newAT = getNewAT(sts, valueN);
    int lvt = getLevelT(sts, valueN);
    showPointAndSpe(valueN, newAT, lvt);
    mapOrder.update('Number of Armature', (value) => valueN.toString());
    mapOrder.update('Level of Armature', (value) => lvlArmts.lvlArmt[lvt]);
  }

  void showPointAndSpe(int valueN, String newAT, int lvt) {
    print('You have most $valueN armatures and $newAT');
    print('Your Level of Armature :');
    print(lvlArmts.lvlArmt[lvt]);
  }

  void showStatus(String? sts, String tel) {
    print('----------');
    print('Member Status: ');
    print(sts);
    String tl = tel.substring(6, 10);
    print('XXX-XXX-$tl');
  }

  int useAsCumtomPoints() {
    final random = Random();
    final valueN = random.nextInt(170);
    return valueN;
  }

  String getNewAT(String? status, int valueN) {
    String statuss = status as String;
    double price = getPriceFromOrder();
    if (valueN >= 350) {
      return 'You get exclusive coupon for exchange your favorite menu and special gift!';
    } else if (valueN >= 150) {
      double cc = calcPointEvery15(price);
      return 'You get $cc Winged Turtle armatures and exclusive coupon for exchange your favorite menu!';
    } else if (statuss.compareTo('-') == 0) {
      double cc = calcPointEvery20(price);
      return 'You get $cc Shyny Turtle armatures!';
    } else if (statuss.compareTo('Member LINE@TAOBIN') == 0) {
      double cc = calcPointEvery15(price);
      return 'You get $cc Totter Turtle armatures and dicount ฿50!';
    }
    return '';
  }

  double calcPointEvery20(double price) {
    Calculate c = Calculate(price, '/', 20);
    double cc = c.calculate().floorToDouble();
    return cc;
  }

  double calcPointEvery15(double price) {
    Calculate c = Calculate(price, '/', 15);
    double cc = c.calculate().floorToDouble();
    return cc;
  }

  double getPriceFromOrder() {
    String priceS = mapOrder['Price'] as String;
    String pm = priceS.substring(1);
    double price = double.parse(pm);
    return price;
  }

  int getLevelT(String? status, int valueN) {
    String statuss = status as String;
    if (valueN >= 350) {
      return 4;
    } else if (valueN >= 150) {
      return 3;
    } else if (statuss.compareTo('-') == 0) {
      return 1;
    } else if (statuss.compareTo('Member LINE@TAOBIN') == 0) {
      return 2;
    }
    return 0;
  }

  void showProcessAfterPayment() {
    print('----------');
    print('Please take your bulb and lid between waiting me');
    print('----------');
    print('----------');
    print('SuccessFul! Please take your menu');
    print('----------');
  }

  void CollectingArmature(String ans, ProcessTAOBIN pMenu) {
    if (ans.compareTo('Y') == 0) {
      print('Please input your telephone number for Collecting Armature: ');
      String tel = stdin.readLineSync()!;
      print('----------');
      getArmatures(tel);
    }
  }

  void confirmOrder(String ans0, ProcessTAOBIN pMenu) {
    if (ans0.compareTo('Y') == 0) {
      getPayment();
      print('= Success Payment =');
      print('----------');
      lvlArmts.lvlArmatureDescribe();
      print('You want to Collecting your Armature? (Y/N): ');
      String ans = stdin.readLineSync()!;
      CollectingArmature(ans, pMenu);
      print('----------');
      getOrder();
      showProcessAfterPayment();
    }
  }

  int choosingCatagoryMenu() {
    int categoryMenu = int.parse(stdin.readLineSync()!);
    print('----------');
    return categoryMenu;
  }

  void thankYouCustomer() {
    print('----------');
    print('Thank you, Have a nice day!');
  }
}

void main() {
  CatagoryMenu catgMenu = CatagoryMenu();
  catgMenu.getCatagoryMenu();
  ProcessTAOBIN pMenu = ProcessTAOBIN();
  int categoryMenu = pMenu.choosingCatagoryMenu();
  pMenu.getMenu(categoryMenu);
  pMenu.getOrder();
  print('You want to confirm order? (Y/N): ');
  String ans0 = stdin.readLineSync()!;
  pMenu.confirmOrder(ans0, pMenu);
  pMenu.thankYouCustomer();
}
