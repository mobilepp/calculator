import 'package:dart_app_calculate/dart_app_calculate.dart'
    as dart_app_calculate;
import 'dart:io';
import "dart:math";

class Calculate {
  var num1 = 0.0;
  var opr = "";
  var num2 = 0.0;
  var result;
  Calculate(double num1, String opr, double num2) {
    this.num1 = num1;
    this.opr = opr;
    this.num2 = num2;
  }
  double calculate() {
    switch (opr) {
      case '+':
        result = num1 + num2;
        break;
      case '-':
        result = num1 - num2;
        break;
      case '*':
        result = num1 * num2;
        break;
      case '/':
        // result = num1 ~/ num2;
        result = num1 / num2;
        break;
      case '%':
        result = num1 % num2;
        break;
      case '^':
        result = pow(num1, num2);
        break;
    }
    // print('result = $result');
    return result;
  }
}

void main() {
  print('Please input first number: ');
  double? num1 = double.parse(stdin.readLineSync()!);
  print('Please input operator: ');
  String opr = stdin.readLineSync()!;
  print('Please input second number: ');
  double? num2 = double.parse(stdin.readLineSync()!);

  Calculate c = Calculate(num1, opr, num2);
  print('result: ');
  print(c.calculate());
}
