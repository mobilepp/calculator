import 'dart_app_calculate.dart'; //for access Calculate
import 'dart:io';

//to be continued at https://gitlab.com/mobilepp/taobin_v2.git

void main() {
  TaoBin taobin = TaoBin();
  taobin.showWelcome();
  taobin.showCategory();
  taobin.inputChoosingCategory();
  taobin.inputChoosingMenu();
  taobin.inputChoosingType();
  taobin.showLevelSweet();
  taobin.inputChoosingLevelSweet();
  taobin.askBulb();
  taobin.askLid();
  taobin.showOrder();
}

class Order implements ShowMap {
  @override
  String nameMenu = 'Now Order';

  @override
  Map sMap = {};

  Map orderMap = {
    'Catagory Menu': '',
    'Menu': '',
    'Type of Menu': '',
    'Level of Sweet': '',
    'Bulb': '',
    'Lid': '',
    'Price': '',
    'Payment': '',
    'Pay': '',
    'Change': '',
    'Number of Armature': '',
    'Level of Armature': ''
  };

  @override
  void showMap() {
    print('$nameMenu of TAO BIN:');
    orderMap.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }
}

class TaoBin with ShowMap {
  int choosingCatg = -1;
  int choosingMenu = -1;
  int choosingType = -1;
  int choosingLevelSweet = -1;
  String bulbYN = '';
  String lidYN = '';
  var orderYN;
  var choosingPayment;

  TaoBin();

  DrinkMana drinkMana = DrinkMana();
  Order order = Order();
  CategoryMenu catName = CategoryMenu('');

  void showWelcome() {
    print('Welcome to happy with TAO BIN!');
    print('----------');
  }

  void showCategory() {
    drinkMana.showMap();
  }

  void inputChoosingCategory() {
    choosingCatg = int.parse(stdin.readLineSync()!);
    order.orderMap
        .update('Catagory Menu', (value) => drinkMana.catg[choosingCatg]);
    showMenu(choosingCatg);
  }

  void showMenu(int choosingCatg) {
    drinkMana.addMenuItem();
    drinkMana.showMenuItem(choosingCatg);
  }

  void inputChoosingMenu() {
    choosingMenu = int.parse(stdin.readLineSync()!);
    CategoryMenu.mn = catName.showMenuAndType(choosingMenu);
    order.orderMap.update('Menu', (value) => CategoryMenu.mn);
    showType(choosingMenu);
  }

  showType(int choosingMenu) {
    drinkMana.showTypeMenu(choosingMenu);
  }

  inputChoosingType() {
    choosingType = int.parse(stdin.readLineSync()!);
    order.orderMap.update('Type of Menu', (value) => choosingType);
  }

  @override
  var nameMenu = 'Sweet\'s Level';
  @override
  var sMap = {
    0: 'No Sugar',
    1: 'Little Sweet',
    2: 'Medium',
    3: 'So Sweet',
    4: 'Very Sweet; Sweet 3 Worlds'
  };

  showLevelSweet() {
    showMap();
  }

  inputChoosingLevelSweet() {
    choosingLevelSweet = int.parse(stdin.readLineSync()!);
    order.orderMap
        .update('Level of Sweet', (value) => sMap[choosingLevelSweet]);
  }

  askBulb() {
    print('Have you want a bulb from TAO BIN (y/n) : ');
    bulbYN = stdin.readLineSync()!;
    order.orderMap.update('Bulb', (value) => bulbYN);
  }

  askLid() {
    print('Have you want a lid from TAO BIN (y/n) : ');
    lidYN = stdin.readLineSync()!;
    order.orderMap.update('Lid', (value) => lidYN);
  }

  showOrder() {
    order.showMap();
  }

  confirmOrder() {}
  showPayment() {}
  inputChoosingPayment() {}
  showStatusPayment() {}
  showStatus() {}
}

abstract class ShowMap {
  var sMap = {};
  var nameMenu = '';
  void showMap() {
    print('$nameMenu of TAO BIN:');
    sMap.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }
}

class CategoryMenu {
  var name = '';
  var menu = <Menu>[];
  var menuName = <int, String>{};

  CategoryMenu(this.name);

  String get getName {
    return name;
  }

  Order order = Order();

  void addMenu(Menu item) {
    menu.add(item);
  }

  void showMenuInCategory() {
    for (var i = 0; i < menu.length; i++) {
      menuName.addAll({i: menu[i].getName});
      // print(menuName);
      stdout.write('$i : ');
      print(menu[i].getName);
    }
    print('----------');
  }

  static String mn = 'm';
  String showMenuAndType(int choosingMenu) {
    // String mn = menuName[choosingMenu] ;
    // print('$mn : check');
    // order.orderMap.update('Menu', (value) => 'mn');
    for (var i = 0; i < menu.length; i++) {
      if (i == choosingMenu) {
        mn = menuName[i] as String;
        print(mn);
        menu[i].showType();
      }
    }
    print('----------');
    return mn;
  }
}

abstract class Menu with Type {
  var name = '';
  @override
  var type = [];
  @override
  var price = [];

  Menu(this.name, this.type, this.price);

  String get getName {
    return name;
  }

  List get getType {
    return type;
  }

  List get getPrice {
    return price;
  }
}

abstract class Type {
  var type = ['Hot', 'Iced', 'Smoothie'];
  var price = [0, 0, 0];
  void showType() {
    for (var i = 0; i < price.length; i++) {
      stdout.write('$i : ');
      stdout.write(type[i]);
      stdout.write(' ฿');
      print(price[i]);
    }
  }
}

class CoffeeMenu extends Menu {
  CoffeeMenu(String name, List<String> type, List<int> price)
      : super(name, type, price);
}

class TeaMenu extends Menu {
  TeaMenu(String name, List<String> type, List<int> price)
      : super(name, type, price);
}

class MilkCCMenu extends Menu {
  MilkCCMenu(String name, List<String> type, List<int> price)
      : super(name, type, price);
}

class PTShakeMenu extends Menu {
  PTShakeMenu(String name, List<String> type, List<int> price)
      : super(name, type, price);
}

class SodaAOthMenu extends Menu {
  SodaAOthMenu(String name, List<String> type, List<int> price)
      : super(name, type, price);
}

class RecommendMenu extends Menu {
  RecommendMenu(String name, List<String> type, List<int> price)
      : super(name, type, price);
}

class DrinkMana implements ShowMap {
  DrinkMana();

  @override
  var nameMenu = 'Category Menu';
  @override
  var sMap = <int, String>{};

  var catg = <int, String>{
    1: 'Coffee',
    2: 'Tea',
    3: 'Milk Cocoa and Caramel',
    4: 'Protein Shake',
    5: 'Soda and Others',
    6: 'Recommended Menu'
  };

  @override
  void showMap() {
    print('$nameMenu of TAO BIN:');
    catg.forEach((key, value) {
      print('$key : $value');
    });
    print('----------');
  }

  CategoryMenu Coffee = CategoryMenu('Coffee');
  CategoryMenu Tea = CategoryMenu('Tea');
  CategoryMenu MilkCC = CategoryMenu('Milk Cocoa and Caramel');
  CategoryMenu PtShake = CategoryMenu('Protein Shake');
  CategoryMenu SodaAOth = CategoryMenu('Soda and Others');
  CategoryMenu Recommend = CategoryMenu('Recommended Menu');

  void addMenuItem() {
    Coffee.addMenu(
        CoffeeMenu('Matcha Latte', ['Hot', 'Iced', 'Smoothie'], [45, 50, 55]));
    Coffee.addMenu(CoffeeMenu('Espresso', ['Hot'], [25]));
    Tea.addMenu(
        TeaMenu('Thai Milk Tea', ['Hot', 'Iced', 'Smoothie'], [35, 40, 45]));
  }

  int mn = -1;

  int showMenuItem(int choosingCatg) {
    switch (choosingCatg) {
      case 1:
        Coffee.showMenuInCategory();
        break;
      case 2:
        Tea.showMenuInCategory();
        break;
      case 3:
        MilkCC.showMenuInCategory();
        break;
      case 4:
        PtShake.showMenuInCategory();
        break;
      case 5:
        SodaAOth.showMenuInCategory();
        break;
      case 6:
        Recommend.showMenuInCategory();
        break;
    }
    mn = choosingCatg;
    return mn;
  }

  void showTypeMenu(int choosingMenu) {
    switch (mn) {
      case 1:
        Coffee.showMenuAndType(choosingMenu);
        break;
      case 2:
        Tea.showMenuAndType(choosingMenu);
        break;
      case 3:
        MilkCC.showMenuAndType(choosingMenu);
        break;
      case 4:
        PtShake.showMenuAndType(choosingMenu);
        break;
      case 5:
        SodaAOth.showMenuAndType(choosingMenu);
        break;
      case 6:
        Recommend.showMenuAndType(choosingMenu);
        break;
    }
  }
}
